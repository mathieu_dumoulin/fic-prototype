#!/usr/bin/env python3
'''
Perform a gridsearch to find the best parameters for the SVM classification algorithm using TF-IDF vectorizer.

Run as a pipeline which vectorizes a collection of text with TF-IDF, then run feature selection with chi2
and finally classifies with SVM. 
'''

import logging
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cross_validation import train_test_split
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import classification_report
from sklearn.svm import SVC
from sklearn.datasets import load_files
import numpy as np
import codecs as cs


# Display progress logs on stdout
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(levelname)s %(message)s')

corpus = "data/corpus6"
label_names = ['not-relevant', 'relevant']

notices = load_files(corpus, categories=label_names, load_content=False)
data = [cs.open(filename, 'r', 'UTF-8').read() for filename in notices.filenames]
n_samples = len(data)
print("text read:{0}".format(n_samples))

count = TfidfVectorizer(stop_words="english", decode_error='replace',
                        ngram_range=(1, 1), strip_accents='unicode',
                        max_df=0.5, min_df=3)
X = count.fit_transform(data)
Y = notices.target

# Split the dataset in two sets, test and train
X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.6) #, random_state=0)
 
# Set the parameters by cross-validation
tuned_parameters = [{'gamma': 10.0 ** np.arange(-4, -1),
                     'C': [0.01,0.1,1.0,10.0,100.0], 
                     'class_weight':[{0:7}], 
                    },                    
                    ]
 
scorings = ['accuracy', 'precision', 'recall', 'f1']

for scoring in scorings:
    print("# Tuning hyper-parameters for %s" % scoring)
    print()
 
    clf = GridSearchCV(SVC(), tuned_parameters, cv=5, scoring=scoring)
    clf.fit(X_train, y_train)
     
    print("Best parameters set found on development set:")
    print()
    print(clf.best_estimator_)
    print()
    print("Grid scores on development set:")
    print()
    for params, mean_score, scores in clf.grid_scores_:
        print("%0.3f (+/-%0.03f) for %r" % (
            mean_score, scores.std() / 2, params))
    print()
     
    print("Detailed classification report:")
    print()
    print("The model is trained on the full development set.")
    print("The scores are computed on the full evaluation set.")
    print()
    y_true, y_pred = y_test, clf.predict(X_test)
    print(classification_report(y_true, y_pred))
    print()

# print('Running the best estimator 10 times')
# 
# clf = SVC(C=10.0, cache_size=200, class_weight={0: 7}, coef0=0.0, degree=3,
#   gamma=0.01, kernel='rbf', max_iter=-1, probability=False,
#   random_state=None, shrinking=True, tol=0.001, verbose=False)
# precisions = []
# recalls = []
# f1s = []
# for _ in range(10):
#     X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.6)
#     clf.fit(X_train, y_train)
#     y_true, y_pred = y_test, clf.predict(X_test)
#     print(classification_report(y_true, y_pred))
#     precisions.append(precision_score(y_true, y_pred))
#     recalls.append(recall_score(y_true, y_pred))
#     f1s.append(f1_score(y_true, y_pred))
#     
# average_precision = sum(precisions)/len(precisions)
# average_recall = sum(recalls)/len(recalls)
# average_f1 = sum(f1s)/len(f1s)
# print('The averaged scores are:')
# print('Precision:',"{:.3f}".format(average_precision))
# print('Recall:', "{:.3f}".format(average_recall))
# print('F1:', "{:.3f}".format(average_f1))
