#!/usr/bin/env python3
'''
Optimize the parameters C and gamma of SVM using GridSearchCV for a collection of text vectorized with BNSTransformer

Search for the best parameter using Cross-validation in a brute-force search of the parameter space. 
'''

import logging
import numpy as np
import codecs as cs
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.cross_validation import train_test_split
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import classification_report
from sklearn.svm import SVC
from sklearn.datasets import load_files

from ficlearn.feature_extraction.text import BnsTransformer

# Display progress logs on stdout
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(levelname)s %(message)s')

corpus = 'data/corpus6'
label_names = ['relevant', 'spam']

notices = load_files(corpus, categories=label_names, load_content=False)
data = [cs.open(filename, 'r', 'UTF-8').read() for filename in notices.filenames]
n_samples = len(data)
print("text read:{0}".format(n_samples))

count = CountVectorizer(max_df=0.5, ngram_range=(1, 1), stop_words='english')
X = count.fit_transform(data)
Y = notices.target

X = BnsTransformer(Y, count.vocabulary_).fit_transform(X)

# Split the dataset in two sets, test and train
X_train, X_test, y_train, y_test = train_test_split(
    X, Y, test_size=0.6, random_state=0)

# Set the parameters by cross-validation
tuned_parameters = [{'gamma': 10.0 ** np.arange(-4, -1),
                     'C': [0.01,0.1,1.0,10.0,100.0], 
                     'class_weight':[{0:1},{0:2},{0:3},{0:4}], 
                    },                    
                    ]
 
scorings = ['accuracy', 'precision', 'recall', 'f1']

for scoring in scorings:
    print("# Tuning hyper-parameters for %s" % scoring)
    print()
 
    clf = GridSearchCV(SVC(), tuned_parameters, cv=5, scoring=scoring)
    clf.fit(X_train, y_train)
    
    print("Best parameters set found on development set:")
    print()
    print(clf.best_estimator_)
    print()
    print("Grid scores on development set:")
    print()
    for params, mean_score, scores in clf.grid_scores_:
        print("%0.3f (+/-%0.03f) for %r" % (
            mean_score, scores.std() / 2, params))
    print()
    
    print("Detailed classification report:")
    print()
    print("The model is trained on the full development set.")
    print("The scores are computed on the full evaluation set.")
    print()
    y_true, y_pred = y_test, clf.predict(X_test)
    print(classification_report(y_true, y_pred))
    print()
