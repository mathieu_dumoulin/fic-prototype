from os.path import dirname
from os.path import join
from os.path import exists
from os.path import expanduser
from os.path import isdir
from os import listdir

container_path='data/corpus6'
print(container_path)

folders = [f for f in sorted(listdir(container_path)) 
           if isdir(join(container_path, f))]
label_names = ['relevant', 'spam']


print(folders)
folders = [f for f in folders if f in label_names]
print(folders)